export enum Environments {
    PROD = 'production',
    DEV = 'development'
}

declare const ENV: string;

export const env = (typeof ENV === 'string' ? ENV : 'development');
export const isDev = env === 'development';
export const isProd = env === 'production';